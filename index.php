<!DOCTYPE HTML>
<html>
<head>
<link href="opt/css/physics.elements.css" rel="stylesheet" type="text/css" />
<script src="opt/js/jquery-1.4.2.js" language="javascript"></script>
<script src="opt/js/jquery.gravity.plugin-0.1a.js" language="javascript"></script>

<title>Pix Mapping Javascript PHP</title>
</head>
<body onload="init();">
<p id="debug" style="position:fixed; bottom:0; left:0; text-align:left"; ></p>

<div  style="position:fixed; bottom:0; right:0; text-align:left; padding:10px; background:#ddd;" >
<form name="myform" method="post">
<p>Gravidade:</p>
<select id="gravidade" onchange="init();">
  <option value="0">Zero</option>
  <option value="0.1">Muito Baixa</option>
  <option value="0.2">Baixa</option>
  <option value="0.3">Mercury</option>
  <option value="0.4">Calisto</option>
  <option value="0.5">Marth</option>
  <option value="0.9" selected="selected">Earth</option>
  <option value="0.7">Europe Moon</option>
  <option value="0.8">Saturn</option>
  <option value="0.9">Jupter</option>
</select>

<p><br />Material:</p>
<select id="material" onchange="init();">
  <option value="1">Materia Escura</option>
  <option value="1.1" selected="selected">Latex</option>
  <option value="1.2">Borracha</option>
  <option value="1.3">Plastico</option>
  <option value="1.5">Madeira</option>
  <option value="1.7">Aluminio</option>
  <option value="2">Ferro</option>
  <option value="3">A&ccedil;o</option>
  <option value="4">Chumbo</option>
</select>
</form>

<p><br />Acelera&ccedil;&atilde;o X/Y:</p>
<select id="xi" onchange="init();">
  <option value="0">0</option>
  <option value="1"  selected="selected">1</option>
  <option value="2">2</option>
  <option value="4">4</option>
  <option value="8">8</option>
  <option value="16">16</option>
  <option value="32">32</option>
  <option value="64">64</option>
</select>

<select id="yi" onchange="init();">
  <option value="0">0</option>
  <option value="1" selected="selected">1</option>
  <option value="2">2</option>
  <option value="4">4</option>
  <option value="8">8</option>
  <option value="16">16</option>
  <option value="32">32</option>
  <option value="64">64</option>
</select>
</form>

</div>

<script type="text/javascript">

	x = 20;
	y = 20;
	r = 10;
	w = 40;
	h = 40;
	//dx = 1;
	//dy = 1;
	//gravity = 0.5;
	//bounce = 2;


	delay = 10;

	function init(){

	dx = parseInt(document.getElementById("xi").value);
	dy = parseInt(document.getElementById("yi").value);
	gravity = parseInt(document.getElementById("gravidade").value);
	bounce = parseInt(document.getElementById("material").value);



		clearInterval('i');

		debug =document.getElementById('debug'); 
		canvas=document.getElementById('mycanvas');

		grid = canvas.getContext('2d');
		cxy = canvas.getContext('2d');
		ctx = canvas.getContext('2d');
		
		canvas.width = 1000;
		canvas.height = 500;

		WORLDX = canvas.width;
		WORLDY = canvas.height;

		//cartesiano();
		drawSquare();

		//drawCircle();

	}


	function square(){

		clear();

		if(y+h<WORLDY) dy+=gravity;



		x+=dx,y+=dy;


		ctx.beginPath();
		ctx.fillStyle='rgba(255,0,0,0.25)';
		ctx.fillRect(x,y,w,h);
		ctx.closePath();
		
		if(parseInt(x+w)>=WORLDX || parseInt(x+dx)<=0) dx=dx/bounce,dx=-dx; 
		if(parseInt(y+h)>=WORLDY || parseInt(y+dy)<=0) dx=dx/bounce,dy=dy/bounce,dy=-dy;
		if(parseInt(y+h)>WORLDY) y=(WORLDY-h-1);
		debug.innerHTML = "DX:"+parseInt(y+h)+"\nDY: "+parseInt(y+dy);
		//cartesiano();



	}


	function drawSquare(){ i = setInterval(square,delay); }








	function clear(){   ctx.clearRect(0,0,WORLDX,WORLDY);  }



/*

	function drawCircle(){

		setInterval(circle,px);

	}








	function circle() {

		//clear();
		
		ctx.beginPath();
		ctx.arc(x, y, r, 0, Math.PI*2, true);
		ctx.closePath();
		ctx.closePath();
		ctx.fill();

		if(x+r>=WORLDX || x-r<=0) dx = -dx; 
		if(y+r>=WORLDY || y-r<=0) dy = -dy; 

		x+=dx;
		y+=dy;
		


	}
*/
	function cartesiano(){

		grid.beginPath();

		for(i=0;i<WORLDX;i+=10){
			grid.moveTo(i,0);
			grid.lineTo(i,WORLDY);
		}

		for(i=0;i<WORLDY;i+=10){
			grid.moveTo(0,i);
			grid.lineTo(WORLDX,i);
		}
		grid.strokeStyle = "#E0E0E0";
		grid.stroke();

		grid.closePath();

		//

		grid.beginPath();
		grid.moveTo(x,0);
		grid.lineTo(x,WORLDY);
		grid.moveTo(x+w,0);
		grid.lineTo(x+w,WORLDY);

		grid.moveTo(0,y);
		grid.lineTo(WORLDX,y);
		grid.moveTo(0,y+h);
		grid.lineTo(WORLDX,y+h);

		grid.strokeStyle = "#eebbbb";
		grid.stroke();
		grid.closePath();


	}		



</script>

<!--
<div class="ball rigid"></div>
<div class="ground rigid"></div>
-->

<canvas id="mycanvas" style="border:1px solid #000;"></canvas>
</body>
</html>
