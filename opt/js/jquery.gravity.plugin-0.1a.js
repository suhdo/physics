

$.fn.gravity = function(settings) {

	var defaults = {
	elemento:"#steel",
	material:'steel',
	pixScale:5, // pixels por metro
	wind:0.5, // metros segundo
	startx:400,
	starty:800,
	randomx:false,
	randomy:false,
	rotationDegrees:5.5,
	ground:".ground" // = 1 metro
	// massiveBodyMass:6000000000000000000000,
	// elementBody:'steel',
	// elementBodyMass:1,
	// centimetersPerPixel:100,
	//density:7860000 
	}

	settings = $.extend(defaults,settings); 

	$this = $(this);
	
	var l = $this.length;
	var lg = $(settings.ground).size();
	
	var i;
	var j;

	var windfx;
	var n = 0;
	var x;
	var y;
	var px;
	var py;
	var epx = Array();
	var epy = Array();
	var ew = Array();
	var eh = Array();
	var w = Array();
	var h = Array();
	var wx = Array();
	var wy = Array();
	var floorX = Array();
	var floorY = Array();

	
	var deg = 0;
	var interv = Array();
	var hit=0;

	var worldx = $('body').css('width');

		for(i=0;i<lg;i++){
			
			w[i] = parseInt($(settings.ground+":eq("+i+")").css('width'));
			h[i] = parseInt($(settings.ground+":eq("+i+")").css('height'));
			wx[i] =  parseInt($(settings.ground+":eq("+i+")").css('left'));
			wy[i] = parseInt($(settings.ground+":eq("+i+")").css('bottom'));

			floorX[i] = w[i]+wx[i];
			floorY[i] = h[i]+wy[i];

			//$(settings.ground+":eq("+i+")").html(w[i]+"x"+h[i]+"="+floorY[i]);
			//alert(floorX[i]);
		}




		for(i=0;i<l;i++){

			x=settings.startx;
			$($this[i]).css('left',x+"px");

			y=settings.starty; 
			$($this[i]).css('bottom',y+"px");

 			epx[i] = x;
 			epy[i] = y;

 			ew[i] = $($this[i]).css('width');
 			eh[i] = $($this[i]).css('height');


		}



	switch(settings.material){

	case "wood":


		
		interv = setInterval(function(){

		for(i=0;i<lg;i++){

			
			deg+=settings.rotationDegrees;
			windfx = settings.wind;

			x+=windfx;
			px = parseInt($($this).css('left'));
			py = parseInt($($this).css('bottom'));

			epx[i] = px;
			epy[i] = py;

		
				
				if((floorY[n]<epy[n])||(epx[n]>(wx[n]+w[n]))||(wx[n]>epx[n])){  y-=settings.pixScale;  }else{  if(n<1) n++; }




				$(settings.ground+":eq("+i+")").html(floorY[n]+" - "+n+" - "+py);




				 $($this[i]).html(epx[i]+"<br />"+epy[i]);


	

				
					$($this[i]).css('bottom',y+"px");
					//if((py>floorY[n])||(px>floorX[n])){ $($this[i]).css('bottom',y+"px"); }else{   n=1; break; }


			$($this).css({'left':x+"px"});
			//$($this).css({"-webkit-transform":"rotate("+deg+"deg)","-moz-transform":"rotate("+deg+"deg)"});


			//if((floorY>epy[i])&&(floorX>epx[i])) {  $($this[i]).css({"-webkit-transform":"rotate(0deg)","-moz-transform":"rotate(0deg)"}); clearInterval(interv); }
		}

		},1);

	break;

	default:

	break;
	
	}



		return $this;




}



